<?php

/*
        Plugin Name: افزونه واژه جادویی
        Plugin URI: http://www.vajehyab.com/magicword
        Plugin Update Check URI: http://www.vajehyab.com/magicword/plugin/q2a/version
        Plugin Description: ترجمه لغات سخت فارسی و انگلیسی در سایت شما به صورت آجاکس قدرت گرفته از Vajehyab.com
        Plugin Version: 1.00
        Plugin Date: 1392-10-08
        Plugin Author: سید امیرحسین طاووسی
        Plugin Author URI: https://github.com/Tavousi
        Plugin License: http://www.vajehyab.com/privacy
*/


	if (!defined('QA_VERSION')) { // don't allow this page to be requested directly from browser
			header('Location: ../../');
			exit;
	}

	qa_register_plugin_layer('qa-share-layer.php', 'Share Javascript Code');	

/*
	Omit PHP closing tag to help avoid accidental output
*/
